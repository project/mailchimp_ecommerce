<?php

/**
 * @file
 * Mailchimp E-commerce integration.
 */

/**
 * Implements hook_page_attachments().
 */
function mailchimp_ecommerce_page_attachments(array &$attachments) {
  // Check for campaign ID as a way to tell if this is the initial page landed
  // on from the email link. The landing site value needs to be that initial
  // url.
  $campaign_id = \Drupal::requestStack()->getCurrentRequest()->get('mc_cid');
  $current_url = \Drupal::request()->getSchemeAndHttpHost() . \Drupal::request()->getRequestUri();
  if (!empty($campaign_id)) {
    $session = \Drupal::request()->getSession();
    $session->set('mc_cid', $campaign_id);
    $session->set('mc_landing_site', $current_url);
  }
}

/**
 * Generates a unique ID used to identify a store integration to Mailchimp.
 *
 * @return string
 *   The unique store ID.
 */
function mailchimp_ecommerce_generate_store_id() {
  return uniqid();
}

/**
 * Gets the store ID of the Mailchimp E-Commerce integration.
 *
 * @return string
 *   The store ID.
 */
function mailchimp_ecommerce_get_store_id() {
  return \Drupal::service('mailchimp_ecommerce.helper')->getStoreId();
}

/**
 * Gets the campaign ID from the current user's session.
 *
 * @return string
 *   The campaign ID.
 */
function mailchimp_ecommerce_get_campaign_id() {
  return \Drupal::service('mailchimp_ecommerce.helper')->getCampaignId();
}

/**
 * Determines if a Mailchimp E-Commerce integration is enabled.
 *
 * @return bool
 *   TRUE if an integration is enabled.
 */
function mailchimp_ecommerce_is_enabled() {
  return \Drupal::service('mailchimp_ecommerce.helper')->isEnabled();
}

/**
 * Determines if customer data is valid.
 *
 * @param array $customer
 *   Array of customer data.
 *
 * @return bool
 *   TRUE if customer data is valid.
 */
function mailchimp_ecommerce_validate_customer(array $customer) {
  return \Drupal::service('mailchimp_ecommerce.helper')->validateCustomer($customer);
}

/**
 * Get the List/Audience ID being used.
 *
 * @return string
 *   The List/Audience ID.
 */
function mailchimp_ecommerce_get_list_id() {
  return \Drupal::config('mailchimp_ecommerce.settings')->get('mailchimp_ecommerce_list_id');
}

/**
 * Returns currency codes from the xml file.
 *
 * This is used if Drupal Commerce is not available.
 *
 * @return array
 *   Array of currency codes.
 */
function mailchimp_ecommerce_get_currency_codes() {
  return \Drupal::service('mailchimp_ecommerce.helper')->getCurrencyCodes();
}

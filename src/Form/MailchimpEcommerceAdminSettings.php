<?php

namespace Drupal\mailchimp_ecommerce\Form;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Link;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\mailchimp_ecommerce\StoreHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Mailchimp Ecommerce setting form.
 */
class MailchimpEcommerceAdminSettings extends ConfigFormBase {

  /**
   * The Store Handler Interface.
   *
   * @var \Drupal\mailchimp_ecommerce\StoreHandlerInterface
   */
  protected $storeHandler;

  /**
   * The Field manager Interface.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $fieldManager;

  /**
   * The Module Handler Interface.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * MailchimpEcommerceAdminSettings constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The Config Factory Interface.
   * @param \Drupal\mailchimp_ecommerce\StoreHandlerInterface $store_handler
   *   The Store Handler Interface.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $field_manager
   *   The Field Manager Interface.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The Module Handler Interface.
   */
  public function __construct(ConfigFactoryInterface $config_factory, StoreHandlerInterface $store_handler, EntityFieldManagerInterface $field_manager, ModuleHandlerInterface $module_handler) {
    parent::__construct($config_factory);

    $this->storeHandler = $store_handler;
    $this->fieldManager = $field_manager;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('mailchimp_ecommerce.store_handler'),
      $container->get('entity_field.manager'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mailchimp_ecommerce_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('mailchimp_ecommerce.settings');

    $product_description = $form_state->getValue(['mailchimp_ecommerce_description']);
    if (!empty($product_description)) {
      $config->set('mailchimp_ecommerce_description', $product_description);
    }

    $product_image = $form_state->getValue(['mailchimp_ecommerce_product_image']);
    if (!empty($product_image)) {
      $config->set('mailchimp_ecommerce_product_image', $product_image);
    }
    $settings = [
      'mailchimp_ecommerce_store_name',
      'mailchimp_ecommerce_list_id',
      'mailchimp_ecommerce_store_id',
      'mailchimp_ecommerce_integration',
      'display_error_messages',
    ];
    foreach ($settings as $variable) {
      if (isset($form[$variable]['#parents'])) {
        $config->set($variable, $form_state->getValue($form[$variable]['#parents']));
      }
    }
    $config->save();

    if (method_exists($this, '_submitForm')) {
      $this->_submitForm($form, $form_state);
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['mailchimp_ecommerce.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config('mailchimp_ecommerce.settings');
    $store_id = $config->get('mailchimp_ecommerce_store_id');
    if ($store_id) {
      $existing_store = $this->storeHandler->getStore($store_id);
    }
    else {
      $existing_store = NULL;
    }
    $form['mailchimp_ecommerce_notice'] = [
      '#markup' => t('This page will allow you to create a store. Once created, you cannot change the audience associated with the store.'),
    ];
    $form['mailchimp_ecommerce_store_name'] = [
      '#type' => 'textfield',
      '#title' => t('Store Name'),
      '#required' => TRUE,
      '#default_value' => $config->get('mailchimp_ecommerce_store_name'),
      '#description' => t('The name of your store as it should appear in your Mailchimp account.'),
    ];

    $mailchimp_lists = mailchimp_get_lists();
    $list_options = ['' => '-- Select --'];

    foreach ($mailchimp_lists as $list_id => $list) {
      $list_options[$list_id] = $list->name;
    }

    if (!empty($config->get('mailchimp_ecommerce_list_id'))) {
      $existing_store_id = $config->get('mailchimp_ecommerce_list_id');
      $form['mailchimp_ecommerce_list_id_existing'] = [
        '#markup' => t('Once created, the audience cannot be changed for a given store. This store is connected to the audience named') . ' ' . $list_options[$existing_store_id],
      ];
    }
    else {
      $form['mailchimp_ecommerce_list_id'] = [
        '#type' => 'select',
        '#title' => t('Store Audience'),
        '#required' => TRUE,
        '#options' => $list_options,
        '#default_value' => $config->get('mailchimp_ecommerce_list_id'),
      ];
    }

    $list_options_currency = ['' => '-- Select --'] + mailchimp_ecommerce_get_currency_codes();
    $form['mailchimp_ecommerce_currency'] = [
      '#type' => 'select',
      '#options' => $list_options_currency,
      '#title' => t('Store Currency Code'),
      '#required' => TRUE,
      '#default_value' => $existing_store ? $existing_store->currency_code : NULL,
      '#description' => t('This is overridden if you have selected to use the default currency from Commerce.'),
    ];

    $image_options = ['' => t('-- Select --')];
    $description_options = ['' => t('-- Select --')];
    $has_images = FALSE;
    $has_description = FALSE;
    $desired_type = '';

    $field_map = $this->fieldManager->getFieldMap();
    if ($this->moduleHandler->moduleExists('mailchimp_ecommerce_commerce')) {
      $desired_type = 'commerce_product';
    }

    foreach ($field_map as $entity_type => $fields) {
      if ($entity_type == $desired_type) {
        foreach ($fields as $field_name => $field_properties) {
          if ($field_properties['type'] == 'image') {
            $image_options[$field_name] = $field_name;
            $has_images = TRUE;
          }
          elseif ($field_properties['type'] == 'text' || $field_properties['type'] == 'text_long' || $field_properties['type'] == 'text_with_summary') {
            $description_options[$field_name] = $field_name;
            $has_description = TRUE;
          }
        }
      }
    }
    if ($has_images) {
      $form['mailchimp_ecommerce_product_image'] = [
        '#type'        => 'select',
        '#title'       => t('Product Image'),
        '#multiple'    => FALSE,
        '#description' => t('Please choose the image field for your products.'),
        '#options'       => $image_options,
        '#default_value' => $config->get('mailchimp_ecommerce_product_image'),
        '#required'      => TRUE,
      ];
    }

    if ($has_description) {
      $form['mailchimp_ecommerce_description'] = [
        '#type'        => 'select',
        '#title'       => t('Product Description'),
        '#multiple'    => FALSE,
        '#description' => t('Please choose the description field for your products.'),
        '#options'       => $description_options,
        '#default_value' => $config->get('mailchimp_ecommerce_description'),
        '#required'      => TRUE,
      ];
    }

    if (!empty($config->get('mailchimp_ecommerce_store_id'))) {
      $form['sync'] = [
        '#type' => 'fieldset',
        '#title' => t('Product sync'),
        '#collapsible' => FALSE,
      ];
      $form['sync']['products'] = [
        '#markup' => Link::fromTextAndUrl(t('Sync existing products to Mailchimp'), Url::fromRoute('mailchimp_ecommerce.sync'))->toString(),
      ];
      $form['sync-orders'] = [
        '#type' => 'fieldset',
        '#title' => t('Order sync'),
        '#collapsible' => FALSE,
      ];
      $form['sync-orders']['orders'] = [
        '#markup' => Link::fromTextAndUrl(t('Sync existing orders to Mailchimp'), Url::fromRoute('mailchimp_ecommerce.sync_orders'))->toString(),
      ];
    }

    $form['platform'] = [
      '#type' => 'hidden',
      '#default_value' => '',
    ];

    $form['display_error_messages'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display Error Messages'),
      '#description' => $this->t('Display API error messages for debugging.'),
      '#default_value' => $config->get('display_error_messages') ?? FALSE,
    ];

    return $form;
  }

  /**
   *
   */
  public function _submitForm(array &$form, FormStateInterface $form_state) {
    $store_id = $this->config('mailchimp_ecommerce.settings')->get('mailchimp_ecommerce_store_id');
    if ($this->config('mailchimp_ecommerce.settings')->get('mailchimp_ecommerce_store_id') == NULL) {
      $store_id = mailchimp_ecommerce_generate_store_id();
      $this->configFactory->getEditable('mailchimp_ecommerce.settings')->set('mailchimp_ecommerce_store_id', $store_id)->save();
    }

    if ($store_id != NULL) {
      $currency = $form_state->getValue(['mailchimp_ecommerce_currency']);

      $platform = !empty($form_state->getValue('platform')) ? $form_state->getValue('platform') : '';

      // Determine if a store is being created or updated.
      $existing_store = $this->storeHandler->getStore($store_id);

      if (empty($existing_store)) {
        $store = [
          'list_id' => !$form_state->getValue(['mailchimp_ecommerce_list_id']) ? $form_state->getValue(['mailchimp_ecommerce_list_id']) : $this->config('mailchimp_ecommerce.settings')->get('mailchimp_ecommerce_list_id'),
          'name' => $form_state->getValue(['mailchimp_ecommerce_store_name']),
          'currency_code' => $currency,
        ];

        $this->storeHandler->addStore($store_id, $store, $platform);
      }
      else {
        $this->storeHandler->updateStore($store_id, $form_state->getValue(['mailchimp_ecommerce_store_name']), $currency, $platform);
      }
    }

  }

}

<?php

namespace Drupal\mailchimp_ecommerce;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\mailchimp_ecommerce\MailchimpEcommerceHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Order handler.
 */
class OrderHandler implements OrderHandlerInterface, ContainerInjectionInterface {

  /**
   * A request stack symfony instance.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The ecommerce helper.
   *
   * @var \Drupal\mailchimp_ecommerce\MailchimpEcommerceHelper
   */
  protected $helper;

  /**
   * OrderHandler constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   A request stack symfony instance.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\mailchimp_ecommerce\MailchimpEcommerceHelper $helper
   *   The Mailchimp ecommerce helper.
   */
  public function __construct(RequestStack $request_stack, LoggerChannelFactoryInterface $logger_factory, MailchimpEcommerceHelper $helper) {
    $this->requestStack = $request_stack;
    $this->loggerFactory = $logger_factory;
    $this->helper = $helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('logger.factory'),
      $container->get('mailchimp_ecommerce.helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getOrder($order_id) {
    try {
      $store_id = mailchimp_ecommerce_get_store_id();
      if (empty($store_id)) {
        throw new \Exception('Cannot get an order without a store ID.');
      }

      /** @var \Mailchimp\MailchimpEcommerce $mc_ecommerce */
      $mc_ecommerce = mailchimp_get_api_object('MailchimpEcommerce');
      $order = $mc_ecommerce->getOrder($store_id, $order_id);
      return $order;
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('mailchimp_ecommerce')->error('Unable to get order: %message', ['%message' => $e->getMessage()]);
      $this->helper->addErrorMessage($e->getMessage());
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function addOrder($order_id, array $customer, array $order) {
    try {
      $store_id = mailchimp_ecommerce_get_store_id();
      if (empty($store_id)) {
        throw new \Exception('Cannot add an order without a store ID.');
      }
      if (!mailchimp_ecommerce_validate_customer($customer)) {
        // A user not existing in the store's Mailchimp list/audience is not an
        // error, so don't throw an exception.
        return;
      }

      // Get the Mailchimp campaign ID, if available.
      $campaign_id = mailchimp_ecommerce_get_campaign_id();
      if (!empty($campaign_id)) {
        $session = $this->requestStack->getCurrentRequest()->getSession();
        $order['landing_site'] = $session->get('mc_landing_site', '');
      }

      /** @var \Mailchimp\MailchimpEcommerce $mc_ecommerce */
      $mc_ecommerce = mailchimp_get_api_object('MailchimpEcommerce');
      $mc_ecommerce->addOrder($store_id, $order_id, $customer, $order);
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('mailchimp_ecommerce')->error('Unable to add an order: %message', ['%message' => $e->getMessage()]);
      $this->helper->addErrorMessage($e->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function updateOrder($order_id, array $order) {
    try {
      $store_id = mailchimp_ecommerce_get_store_id();
      if (empty($store_id)) {
        throw new \Exception('Cannot update an order without a store ID.');
      }

      /** @var \Mailchimp\MailchimpEcommerce $mc_ecommerce */
      $mc_ecommerce = mailchimp_get_api_object('MailchimpEcommerce');
      $mc_ecommerce->updateOrder($store_id, $order_id, $order);
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('mailchimp_ecommerce')->error('Unable to update an order: %message', ['%message' => $e->getMessage()]);
      $this->helper->addErrorMessage($e->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildOrder(Order $order, array $customer) {
    $order_items = $order->getItems();
    $lines = [];

    /** @var \Drupal\commerce_order\Entity\OrderItem $order_item */
    foreach ($order_items as $order_item) {
      $line = [
        'id' => $order_item->id(),
        'product_id' => $order_item->getPurchasedEntity()->getProductId(),
        // @todo Figure out how to differentiate between product and variant ID
        // here.
        'product_variant_id' => $order_item->getPurchasedEntityId(),
        'quantity' => (int) $order_item->getQuantity(),
        'price' => $order_item->getUnitPrice()->getNumber(),
      ];

      $lines[] = $line;
    }

    $order_data = [
      'customer' => $customer,
      'processed_at_foreign' => date('c'),
      'lines' => $lines,
    ];

    if (isset($customer['address'])) {
      $order_data['billing_address'] = $customer['address'];
    }

    if (!empty($order->getTotalPrice())) {
      $order_data['currency_code'] = $order->getTotalPrice()->getCurrencyCode();
      $order_data['order_total'] = $order->getTotalPrice()->getNumber();
    }

    return $order_data;
  }

  /**
   * {@inheritdoc}
   */
  public function buildProduct(OrderItem $order_item) {

    return [
      'id' => $order_item->id(),
      'product_id' => $order_item->getPurchasedEntity()->getProductId(),
      // @todo Figure out how to differentiate between product and variant ID
      // here.
      'product_variant_id' => $order_item->getPurchasedEntityId(),
      'quantity' => (int) $order_item->getQuantity(),
      'price' => $order_item->getUnitPrice()->getNumber(),
    ];
  }

}

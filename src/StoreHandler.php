<?php

namespace Drupal\mailchimp_ecommerce;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\mailchimp_ecommerce\MailchimpEcommerceHelper;

/**
 * Store handler.
 */
class StoreHandler implements StoreHandlerInterface {

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The ecommerce helper.
   *
   * @var \Drupal\mailchimp_ecommerce\MailchimpEcommerceHelper
   */
  protected $helper;

  /**
   * StoreHandler constructor.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\mailchimp_ecommerce\MailchimpEcommerceHelper $helper
   *   The Mailchimp ecommerce helper.
   */
  public function __construct(LoggerChannelFactoryInterface $logger_factory, MailchimpEcommerceHelper $helper) {
    $this->loggerFactory = $logger_factory;
    $this->helper = $helper;
  }

  /**
   * {@inheritdoc}
   */
  public function getStore($store_id) {
    $store = NULL;
    try {
      /** @var \Mailchimp\MailchimpEcommerce $mc_ecommerce */
      $mc_ecommerce = mailchimp_get_api_object('MailchimpEcommerce');
      $store = $mc_ecommerce->getStore($store_id);
    }
    catch (\Exception $e) {
      if ($e->getCode() == 404) {
        // Store doesn't exist; no need to log an error.
      }
      else {
        $this->loggerFactory->get('mailchimp_ecommerce')->error('Unable to get store: %message', ['%message' => $e->getMessage()]);
        $this->helper->addErrorMessage($e->getMessage());
      }
    }

    return $store;
  }

  /**
   * {@inheritdoc}
   */
  public function addStore($store_id, $store, $platform) {
    try {
      /** @var \Mailchimp\MailchimpEcommerce $mc_ecommerce */
      $mc_ecommerce = mailchimp_get_api_object('MailchimpEcommerce');

      $parameters = [
        'platform' => $platform,
      ];

      $mc_store = $mc_ecommerce->addStore($store_id, $store, $parameters);

      \Drupal::moduleHandler()->invokeAll('mailchimp_ecommerce_add_store', [$mc_store]);
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('mailchimp_ecommerce')->error('Unable to add a new store: %message', ['%message' => $e->getMessage()]);
      $this->helper->addErrorMessage($e->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function updateStore($store_id, $name, $currency_code, $platform) {
    try {
      /** @var \Mailchimp\MailchimpEcommerce $mc_ecommerce */
      $mc_ecommerce = mailchimp_get_api_object('MailchimpEcommerce');

      $parameters = [
        'platform' => $platform,
      ];

      $mc_ecommerce->updateStore($store_id, $name, $currency_code, $parameters);
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('mailchimp_ecommerce')->error('Unable to update a store: %message', ['%message' => $e->getMessage()]);
      $this->helper->addErrorMessage($e->getMessage());
    }
  }

}

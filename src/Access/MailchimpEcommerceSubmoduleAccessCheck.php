<?php

namespace Drupal\mailchimp_ecommerce\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Routing\Access\AccessInterface;

/**
 * Checks that a mailchimp_ecommerce submodule is enabled.
 */
class MailchimpEcommerceSubmoduleAccessCheck implements AccessInterface {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * MailchimpEcommerceSubmoduleAccessCheck constructor.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(ModuleHandlerInterface $module_handler) {
    $this->moduleHandler = $module_handler;
  }

  /**
   * Access check for mailchimp_ecommerce submodules.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access() {
    return AccessResult::allowedIf($this->moduleHandler->moduleExists('mailchimp_ecommerce_commerce'));
  }

}

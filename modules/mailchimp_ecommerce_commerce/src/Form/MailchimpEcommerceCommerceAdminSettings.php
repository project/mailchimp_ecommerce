<?php

namespace Drupal\mailchimp_ecommerce_commerce\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\mailchimp_ecommerce\Form\MailchimpEcommerceAdminSettings;
use Drupal\mailchimp_ecommerce\StoreHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Mailchimp Ecommerce Commerce setting form.
 */
class MailchimpEcommerceCommerceAdminSettings extends MailchimpEcommerceAdminSettings {

  /**
   * The Store Context Interface.
   *
   * @var \Drupal\commerce_store\CurrentStoreInterface
   */
  protected $storeContext;

  /**
   * The Store Handler Interface.
   *
   * @var \Drupal\mailchimp_ecommerce\StoreHandler
   */
  protected $storeHandler;

  /**
   * MailchimpEcommerceAdminSettings constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The Config Factory Interface.
   * @param mixed $store_context
   *   The Store Context Interface, or empty string if none.
   * @param \Drupal\mailchimp_ecommerce\StoreHandlerInterface $store_handler
   *   The Store Handler Interface.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $field_manager
   *   The Field Manager Interface.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The Module Handler Interface.
   */
  public function __construct(ConfigFactoryInterface $config_factory, $store_context, StoreHandlerInterface $store_handler, EntityFieldManagerInterface $field_manager, ModuleHandlerInterface $module_handler) {
    parent::__construct($config_factory, $store_handler, $field_manager, $module_handler);

    $this->storeContext = $store_context;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $module_handler = $container->get('module_handler');

    if ($module_handler->moduleExists('commerce')) {
      $current_store = $container->get('commerce_store.current_store');
    }
    else {
      $current_store = '';
    }

    return new static(
      $container->get('config.factory'),
      $current_store,
      $container->get('mailchimp_ecommerce.store_handler'),
      $container->get('entity_field.manager'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    // This is the currently active store according to Drupal Commerce.
    // Commerce allows multiple stores in D8 - may need to consider that here.
    $store = $this->storeContext->getStore();

    if (!empty($store)) {
      // Set default currency code for the Mailchimp store.
      $default_currency = $store->getDefaultCurrencyCode();
      if (isset($form['mailchimp_ecommerce_currency']['#options'][$default_currency])) {
        $form['mailchimp_ecommerce_currency']['#default_value'] = $default_currency;
      }
    }

    // Identify Drupal Commerce to Mailchimp.
    $form['platform']['#default_value'] = 'Drupal Commerce';

    return $form;
  }

}

<?php

namespace Drupal\mailchimp_ecommerce_commerce\Form;

use Drupal\mailchimp_ecommerce\Form\MailchimpEcommerceSync;

/**
 * Form for synching products.
 */
class MailchimpEcommerceCommerceSync extends MailchimpEcommerceSync {

  /**
   * {@inheritdoc}
   */
  public function _submitForm($form, $form_state) {
    if (!empty($form_state->getValue('sync_products'))) {
      $batch = [
        'title' => t('Adding products to Mailchimp'),
        'operations' => [],
      ];

      $query = \Drupal::entityQuery('commerce_product');
      $result = $query->accessCheck(FALSE)->execute();

      if (!empty($result)) {
        $product_ids = array_keys($result);

        $batch['operations'][] = [
          '\Drupal\mailchimp_ecommerce_commerce\BatchSyncProducts::syncProducts',
          [$product_ids],
        ];
      }

      batch_set($batch);
    }
  }

}

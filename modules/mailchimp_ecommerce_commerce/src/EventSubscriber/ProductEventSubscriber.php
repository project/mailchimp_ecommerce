<?php

namespace Drupal\mailchimp_ecommerce_commerce\EventSubscriber;

use Drupal\commerce_product\Event\ProductEvent;
use Drupal\commerce_product\Event\ProductEvents;
use Drupal\mailchimp_ecommerce\ProductHandler;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event Subscriber for Commerce Products.
 */
class ProductEventSubscriber implements EventSubscriberInterface {

  /**
   * The Product Handler.
   *
   * @var \Drupal\mailchimp_ecommerce\ProductHandler
   */
  private $productHandler;

  /**
   * ProductEventSubscriber constructor.
   *
   * @param \Drupal\mailchimp_ecommerce\ProductHandler $product_handler
   *   The Product Handler.
   */
  public function __construct(ProductHandler $product_handler) {
    $this->productHandler = $product_handler;
  }

  /**
   * Respond to event fired after saving a new product.
   */
  public function productInsert(ProductEvent $event) {
    /** @var \Drupal\commerce_product\Entity\Product $product */
    $product = $event->getProduct();

    $product_id = $product->get('product_id')->value;
    $title = (!empty($product->get('title')->value)) ? $product->get('title')->value : '';
    // @todo Fix Type.
    $type = (!empty($product->get('type')->value)) ? $product->get('type')->value : '';

    $variants = $this->productHandler->buildProductVariants($product);
    $url = $this->productHandler->buildProductUrl($product);
    $image_url = $this->productHandler->getProductImageUrl($product);
    $description = $this->productHandler->getProductDescription($product);

    $this->productHandler->addProduct($product_id, $title, $url, $image_url, $description, $type, $variants);
  }

  /**
   * Respond to event fired after updating an existing product.
   */
  public function productUpdate(ProductEvent $event) {
    $product = $event->getProduct();

    $title = (!empty($product->get('title')->value)) ? $product->get('title')->value : '';
    // @todo Fix Type.
    $type = (!empty($product->get('type')->value)) ? $product->get('type')->value : '';

    $variants = $this->productHandler->buildProductVariants($product);
    $url = $this->productHandler->buildProductUrl($product);
    $image_url = $this->productHandler->getProductImageUrl($product);
    $description = $this->productHandler->getProductDescription($product);

    // Update the existing product and variant.
    $this->productHandler->updateProduct($product, $title, $url, $image_url, $description, $type, $variants);
  }

  /**
   * Respond to event fired after deleting a product.
   */
  public function productDelete(ProductEvent $event) {
    // @todo Process deleted product.
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[ProductEvents::PRODUCT_INSERT][] = ['productInsert'];
    $events[ProductEvents::PRODUCT_UPDATE][] = ['productUpdate'];
    $events[ProductEvents::PRODUCT_DELETE][] = ['productDelete'];

    return $events;
  }

}
